import $ from "jquery";
import "./main.scss";

function importAll(r) {
    return r.keys().map(r);
  }
  
  const images = importAll(require.context('./img', false, /\.(png|jpe?g|svg)$/));


$(document).ready(function () {
    $(".btn").click((e) => {
        alert("Hello Webpack XOXO!");
    });
});
