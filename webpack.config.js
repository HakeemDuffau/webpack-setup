"use strict";

const isDevMode = true;
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: "./src/index.js",
    context: path.resolve(__dirname),
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: "bundle.js",
    },
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/i,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: { sourceMap: isDevMode },
                    },
                    "css-loader",
                    {
                        loader: "postcss-loader",
                        options: {
                            sourceMap: isDevMode,
                            config: {
                                path: "postcss.config.js",
                            },
                        },
                    },
                    "sass-loader",
                ],
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                loader: "file-loader",
                options: {
                    context: path.resolve(__dirname, "src/img"),
                    outputPath: "img",
                    name: "[path][name].[ext]",
                },
            },

            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: isDevMode ? "[name].css" : "[name].[hash].css",
            chunkFilename: isDevMode ? "[id].css" : "[id].[hash].css",
        }),
        new HtmlWebpackPlugin({ template: "./src/index.html" }),
    ],

    devServer: {
        contentBase: "./dist",
    },
};
